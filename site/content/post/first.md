---
layout: blog
title: "Let's Party"
date: 2018-07-27T13:19:04-05:00
draft: false
---

“Peace of mind produces right values, right values produce right thoughts. Right thoughts produce right actions and right actions produce work which will be a material reflection for others to see of the serenity at the center of it all.”
― Robert M. Pirsig, Zen and the Art of Motorcycle Maintenance